<div class="row">
    <div
        class="col-md-12 text-white text-center bg"
        style="padding:80px;background-image:url('<?php echo base_url('assets/img/slider-bg.jpg'); ?>')">
        <h1 class="display-4">Register</h1>
    </div>
</div>
<div class="row bg-light p-5">
    <div class="col-md-6 bg-white mx-auto text-dark p-5">
        <strong>
            <h2>Please Complete all Fields.</h2>
        </strong>
        <hr/>
        <?php echo $status; ?>
        <form method="post" action="">
            <label for="" class="mt-3">First name</label>
            <input
                type="text"
                class="form-control"
                placeholder="First name"
                name="firstname" required/>
            <label for="" class="mt-3">Last name</label>
            <input type="txt" class="form-control" placeholder="Last name" name="lastname"/>
            <label for="" class="mt-3">DOB</label>
            <input type="date" class="form-control" placeholder="DD/MM/YY" name="dob" required/>
            <label for="" class="mt-3">Email</label>
            <input type="txt" class="form-control" placeholder="email" name="email" required/>
            <label for="" class="mt-3">Password</label>
            <div style="position:relative;">
            <input
                type="password"
                class="form-control"
                placeholder="password"
                name="password"
                id="password"
                pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" 
                title="Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters"
                required
                />
                <i id="eye" class="fas fa-eye" style="position:absolute; top:10px; right:10px;"></i>
                <i id="eye1" class="fas fa-eye-slash" style="position:absolute; top:10px; right:10px;"></i>
            </div>
            <label for="" class="mt-3">Password Confirmation</label>
            <input
                type="password"
                class="form-control"
                placeholder="passwordconf"
                name="passwordconf"
                id="passwordconf"
                required
                />
            <label for="" class="mt-3">Qualification</label>
            <select name="qualification" class="form-control">
                <option>10th</option>
                <option>12th</option>
                <option>Diploma</option>
                <option>Graduation</option>
                <option>Post graduation</option>
            </select>
            <label for="" class="mt-3">Gender</label>
            <select name="gender" class="form-control">
                <option>Male</option>
                <option>Female</option>
                <option>others</option>
            </select>
            <label for="" class="mt-3">Address</label>
            <input type="address" class="form-control" placeholder="address" name="address" required>
            <button id="btn" class="btn btn-danger form-control mt-5" type="submit">
                Register</button>
            <div class="mt-3 text-center">
                <i class="fas fa-user"></i>
                Already exists?
                <a href="<?php echo base_url('index.php/home/login')?>">Login Here</a>
            </div>
            <button type="button" onclick="googlelogin()">Sign up using google</button>
            <button type="button" onclick="fblogin()">Sign up using fb</button>
        </div>
    </div>
</form>
<!-- 3rd row end -->
<div class="row">
    <div
        class="col-md-12  text-white text-center bg1"
        style="padding:8%;background-image:url('<?php echo base_url('assets/img/bg1.jpg');?>');">
        <h1>All Govt.Recruitments</h1>
        <p>Instead of being unsatisfied in your job, find a role that you love going to
            each day! Find fulfillment by doing meaningful work for your community.Now is a
            great time to pursue a career in the public sector, because there are not enough
            applicants to fill the open jobs. Take advantage of this, apply for a government
            job today!</p>
        <br/>
        <a href="" class="btn btn-danger btn-lg mt-2">Read More</a>
    </p>

</div>
</div>
<!-- 9th row end -->
<div class="row bg-dark">
<div class="col-md-9 text-white text-center mt-5 mx-auto">
    <h5 >Copyright © 2019 All Govt.Recruitments - All Rights Reserved.</h5>
</div>
<div class="col-md-3 text-white mt-5 mx-auto text-center">
    <h6>Follow us on</h6>
    <a href="https://www.facebook.com/" class="custm">
        <div
            class=" box text-center"
            style="width:30px; height:30px;line-height:30px;background-color:white;border-radius:50%;">
            <i class="fab fa-facebook-f" style="color:black;"></i>
        </div>
    </a>
    <a href="https://www.google.com/" class="custm">
        <div
            class=" box text-center"
            style="width:30px; height:30px;line-height:30px;background-color:white; border-radius:50%;">
            <i class="fab fa-google text-black"></i>
        </div>
    </a>
    <a href="https://twitter.com/">
        <div
            class=" box text-center"
            style="width:30px; height:30px;line-height:30px;background-color:white;border-radius:50%;">
            <i class="fab fa-twitter text-black"></i>
        </div>
    </a>
    <a href="https://www.instagram.com/" class="custm">
        <div
            class=" box text-center"
            style="width:30px; height:30px;line-height:30px;background-color:white;border-radius:50%;">
            <i class="fab fa-instagram text-black"></i>
        </div>
    </a>
</div>
</div>
</div>
<script>
    
        
    

    function matchdata(){
        let pass = $('#password').val();
        let conf = $('#passwordconf').val();
        if(pass==conf){
            $('#password').removeClass('border-danger');
            $('#passwordconf').removeClass('border-danger');
            $('#btn').removeAttr('disabled');

        }else{
            $('#btn').attr('disabled','disabled');
            $('#password').addClass('border-danger');
            $('#passwordconf').addClass('border-danger');
        }
    }
    $(function(){
        // $('input[name="firstname"]').keyup(function(){alert('WORK')})
        $('#eye1').hide();
        $('#password').keyup(function(){
            matchdata();
        });
        $('#passwordconf').keyup(function(){
            matchdata();
        });
        $('#eye').click(function(){
            $('#eye1').show();
            $('#eye').hide();
            $('#password').attr('type','text');
        })
        $('#eye1').click(function(){
            $('#eye').show();
            $('#eye1').hide();
            $('#password').attr('type','password');
        })
        
    })
</script>
<!-- The core Firebase JS SDK is always required and must be listed first -->
<script src="https://www.gstatic.com/firebasejs/6.6.1/firebase-app.js"></script>

<!-- TODO: Add SDKs for Firebase products that you want to use
     https://firebase.google.com/docs/web/setup#config-web-app -->
<script src="https://www.gstatic.com/firebasejs/6.6.1/firebase-auth.js"></script>
<script>
  // Your web app's Firebase configuration
  var firebaseConfig = {
    apiKey: "AIzaSyBCmoMLGif6scT7VE-hShTAt2WMEBBYGgk",
    authDomain: "allgovt.firebaseapp.com",
    databaseURL: "https://allgovt.firebaseio.com",
    projectId: "allgovt",
    storageBucket: "",
    messagingSenderId: "31403930575",
    appId: "1:31403930575:web:235c33095f3d3787b818ba"
  };
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);
</script>
<script>
function googlelogin(){
    let provider = new firebase.auth.GoogleAuthProvider();

    firebase.auth().signInWithPopup(provider)
    .then(function(result){
        console.log(result);
        let email = result.additionalUserInfo.profile.email;
        $('input[name="email"]').val(email);
    })
    .catch(function(err){
        console.log(err);
    })
}
function fblogin(){
    let provider = new firebase.auth.FacebookAuthProvider();
    firebase.auth().signInWithPopup(provider)
    .then(function(result) {
        console.log(result);
    })
    .catch(function(err){
        console.log(err);
    })
}
</script>