<div class="row">
    <div
        class="col-md-12 text-white text-center bg"
        style="padding:80px;background-image:url('<?php echo base_url('assets/img/slider-bg.jpg'); ?>')">
        <h1 class="display-4">LOGIN</h1>
    </div>
</div>

<div class="row bg-light p-5">
    <div class="col-md-6 bg-white mx-auto text-dark" style="padding:5%;">
        <form method="post" action="">
            <strong>
                <h2>User Login</h2>
            </strong>
            <hr/>

            <label for="" class="mt-3">Email</label>
            <input
                type="email"
                class="form-control"
                placeholder="Enter Email Here"
                name="useremail"
                required />
            <label for="" class="mt-3">Password</label>
            <a href="" style="margin-left:20%;">Forgot password?</a>
            <input
                type="password"
                class="form-control"
                placeholder="password"
                name="password"
                required />
            <button type="button" id="login" class="btn btn-danger form-control mt-5" name="login">Login
            </button>
            <div class="mt-3 text-center">
                <i class="fas fa-user"></i>New User?
                <a href="<?php echo base_url('index.php/home/register')?>">
                    Register Here</a>
            </div>
        </form>
    </div>
</div>
<div class="row">
    <div
        class="col-md-12  text-white text-center bg1"
        style="padding:8%;background-image:url('<?php echo base_url('assets/img/bg1.jpg');?>');">
        <h1>All Govt.Recruitments</h1>
        <p>Instead of being unsatisfied in your job, find a role that you love going to
            each day! Find fulfillment by doing meaningful work for your community.Now is a
            great time to pursue a career in the public sector, because there are not enough
            applicants to fill the open jobs. Take advantage of this, apply for a government
            job today!</p>
        <br/>
        <a href="" class="btn btn-danger btn-lg mt-2">Read More</a>
    </p>

</div>
</div>
<!-- 9th row end -->
<div class="row bg-dark">
<div class="col-md-9 text-white text-center mt-5 mx-auto">
    <h5 >Copyright © 2019 All Govt.Recruitments - All Rights Reserved.</h5>
</div>
<div class="col-md-3 text-white mt-5 mx-auto text-center">
    <h6>Follow us on</h6>
    <a href="https://www.facebook.com/" class="custm">
        <div
            class=" box text-center"
            style="width:30px; height:30px;line-height:30px;background-color:white;border-radius:50%;">
            <i class="fab fa-facebook-f" style="color:black;"></i>
        </div>
    </a>
    <a href="https://www.google.com/" class="custm">
        <div
            class=" box text-center"
            style="width:30px; height:30px;line-height:30px;background-color:white; border-radius:50%;">
            <i class="fab fa-google text-black"></i>
        </div>
    </a>
    <a href="https://twitter.com/">
        <div
            class=" box text-center"
            style="width:30px; height:30px;line-height:30px;background-color:white;border-radius:50%;">
            <i class="fab fa-twitter text-black"></i>
        </div>
    </a>
    <a href="https://www.instagram.com/" class="custm">
        <div
            class=" box text-center"
            style="width:30px; height:30px;line-height:30px;background-color:white;border-radius:50%;">
            <i class="fab fa-instagram text-black"></i>
        </div>
    </a>
</div>
</div>
</div>
<script>
$(function(){
    $('#login').click(function(){
        let email = $('input[name="useremail"]').val();
        let password = $('input[name="password"]').val();
        $.ajax({
            url:'<?php echo base_url('index.php/home/checklogin'); ?>',
            type: 'post',
            data: {email:email, password:password},
            success: function(result){
                if(result=='Success'){
                    window.location = "<?php echo base_url('index.php/login'); ?>"
                }else{
                    alert('Invalid email or password.');
                }
            }
        })
    })
})
</script>