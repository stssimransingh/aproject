<?php 
class Home extends CI_Controller{
    function index(){
        $this->load->view('includes/header.php');
        $this->load->view('homepage.php');
        $this->load->view('includes/footer.php');
    }
function about(){
    $this->load->view('includes/header.php');
    $this->load->view('aboutpage.php');
    $this->load->view('includes/footer.php');
}
function contact(){
    $this->load->view('includes/header.php');
    $this->load->view('contact.php');
    $this->load->view('includes/footer.php');
}
function login(){
    $this->load->view('includes/header.php');
    $this->load->view('login.php');
    $this->load->view('includes/footer.php');
}
function register(){
    $status['status'] = "";
    $this->load->model('dbquery');
    if($this->input->post()){
        $data=$this->input->post();
        if($data['password'] == $data['passwordconf']){
            $check = $this->dbquery->checkemailexist($data['email']);
            if($check > 0){
                $status['status'] = "Email address already exist.";
            }else{   
                unset($data['passwordconf']);
                // print_r($data);
                $data['password'] = md5($data['password']);
                if($this->db->insert('ruser',$data)){
                    $status['status'] = "Success";
                }
                else{
                    $status['status'] = "Error While insert";
                }
            }
        }
        else{
            $status['status'] = "Password and Confirm Password Not Match.";
        }
        
    }
    $this->load->view('includes/header.php');
    $this->load->view('register.php', $status);
    $this->load->view('includes/footer.php');
}
function checklogin(){
    $email = $this->input->post('email');
    $password = $this->input->post('password');

    $this->db->where('email',$email);
    $this->db->where('password',md5($password));
    $data = $this->db->get('ruser');
    $userdata = $data->result_array();
    unset($userdata[0]['password']);
    
    if($data->num_rows() > 0){
        $this->session->set_userdata('login','true');
        $this->session->set_userdata('userdata', $userdata[0]);
        echo "Success";
    }else{
        $this->session->set_userdata('login','false');
        echo "Invalid";
    }
}
}

?>