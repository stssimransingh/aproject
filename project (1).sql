-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Sep 13, 2019 at 07:36 AM
-- Server version: 10.3.16-MariaDB
-- PHP Version: 7.2.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `project`
--

-- --------------------------------------------------------

--
-- Table structure for table `ruser`
--

CREATE TABLE `ruser` (
  `id` int(11) NOT NULL,
  `firstname` varchar(256) NOT NULL,
  `lastname` varchar(256) NOT NULL,
  `dob` varchar(256) NOT NULL,
  `email` varchar(256) NOT NULL,
  `password` varchar(256) NOT NULL,
  `qualification` varchar(256) NOT NULL,
  `gender` varchar(256) NOT NULL,
  `address` varchar(256) NOT NULL,
  `usertype` varchar(256) NOT NULL DEFAULT 'user',
  `datetime` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ruser`
--

INSERT INTO `ruser` (`id`, `firstname`, `lastname`, `dob`, `email`, `password`, `qualification`, `gender`, `address`, `usertype`, `datetime`) VALUES
(1, 'Shashank', 'Bhardwaj', '2019-12-31', 'bhardwajshashank44@gmail.com', 'admin', '10th', 'Male', 'H.no 475 power colony ropar', 'user', '2019-09-13 04:44:14'),
(2, 'sonia', 'Bhardwaj', '2019-12-31', 'nehubhatoa12@gmail.com', '21232f297a57a5a743894a0e4a801fc3', '10th', 'Male', 'V.P.O thopia, tehsil balachaur,distt. S.B.S Nagar, Nawanshar', 'user', '2019-09-13 04:48:54'),
(3, 'Shashank', 'Bhardwaj', '2019-01-01', 'bhardwajshashank44@gmail.com', '21232f297a57a5a743894a0e4a801fc3', '10th', 'Male', 'H.no 475 power colony ropar', 'user', '2019-09-13 04:49:52'),
(4, 'Shashank', 'Bhardwaj', '2019-01-01', 'bhardwajshashank4@gmail.com', '21232f297a57a5a743894a0e4a801fc3', '10th', 'Male', 'H.no 475 power colony ropar', 'user', '2019-09-13 04:57:45'),
(5, '', '', '', '', 'd41d8cd98f00b204e9800998ecf8427e', '10th', 'Male', '', 'user', '2019-09-13 05:00:34');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ruser`
--
ALTER TABLE `ruser`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ruser`
--
ALTER TABLE `ruser`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
